## 概要
Amazonの運営するAWSデベロッパー向けコ・ワーキングスペース(AWS Loft)へ行ってきたので共有

* 住所：東京都品川区上大崎 3-1-1 目黒セントラルスクエア 17F
* 最寄：目黒駅徒歩 1 分
* 営業時間：平日（月〜金） 10:00 〜 18:00（最終受付17:30）
* https://aws.amazon.com/jp/start-ups/loft/tokyo/

## 利用条件とルール
### 利用条件
* AWSアカウントを持っていること(無料の個人アカウントでも可)
### ルール
* 昼寝禁止
* 飲食可。ただしアルコール類の持ち込みは禁止
* ドレスコードはカジュアル。ネクタイ禁止

## 設備
* Wifi
* 1人席、4人テーブル、6人テーブル
* 6人用会議室 8（当日受付にて1時間単位で予約できる。無料）
* 電話ブース 2
* 無料ウォーターサーバー
* 自動販売機（ちょっと安い）
* Cafe（有料、支払はクレジットカード or AmazonPay）
* スナック、500円弁当の販売あり
* お手洗い（部屋の外に出る必要あり）
* AWSエキスパートが常駐する相談ブース

## 入室方法
### 初回
* 3階オフィスエントランスで警備員さんにAWS Loft利用と伝える
* エレベーターで17階へ
* 受付で初利用申告
* QRコード出されるので読み取ってAWSにログイン。マイアカウント画面を見せる
* タブレットで会員登録する

### 2回目以降
* タブレットで登録したメールアドレスを入力
* 本人確認証（免許証・保険証・社員証など）を見せる

### 初回の注意
* KDのAWSアカウントだと権限がなくてマイアカウント画面が表示できず、一旦1Dayパスで入室して、後ほど会員登録した
* ↑のためか、名刺2枚を求められた。初回は持って行ったほうがいいかも
* スマホでAWSコンソールにloginする必要があるので、PCでないとパスワード確認できない人は注意

## メモ

* BGMの音量が割と大きめ。近くの席の人の会話も割と聞こえるので、気になる人はノイズキャンセリングイヤホンがあるといいかも
* グループで仕事場がわりに使っているスタートアップの人が結構いる
* 12時くらいになるとほぼ満席に。集中して作業したい場合は早めの時間の利用がおすすめ
* コーヒーの蓋が紙で、紙の味がして美味しくない。蓋を外せばいける
* AmazonPayは、スマホのAmazonアプリのハンバーガーメニューから 「プログラムと特典 > すべてを見る > AmazonPay」

## Photos
ワーキングスペース  
![enter image description here](https://lh3.googleusercontent.com/qH8oZZ6_i7urjtPk73N29spWopFJygqRqKTaOhfM4IFRvd8LjMJqZjnX8FjCP71b5rLvlEZmqJM)
![enter image description here](https://lh3.googleusercontent.com/sEFrIj9qmt5DtZ8fDvnJREADjrzJCp-fj7PhTHRsYc4_ilji6s8SgefIIYWPbkjQmIhxvxj0qsw)
![enter image description here](https://lh3.googleusercontent.com/BDmFEfy9i1HZ5J_M42jnbZf6E982dM9pp_slUTuAkpIiuYO3Ob4KTVyEgCbFPmSH6RUQ_5zrxDU)


カフェ・自動販売機・ウォーターサーバー  
![enter image description here](https://lh3.googleusercontent.com/gZJdyCVSohW9ccMAZ-dtBV9IlwdP8g4oZrMTRKXdPKBliiculLZq6sg0QZDDpoG5y46BTuBcdWk)
![enter image description here](https://lh3.googleusercontent.com/6LYlmSWMuwlVHiBQ4ViikXhUpC4aGrJpnf0BEaarHQgLWQAS0_RXYBGG_UZyyQdNjh7yshgMoIE)
![enter image description here](https://lh3.googleusercontent.com/wWJLPY25gI_0dOaXmLnm6Kt1ATKUTQ0DbyEId4pvEcn40KZ0YNW0FiW77ovtqgPGL1znJf7SaJ4)
![enter image description here](https://lh3.googleusercontent.com/H5jCJx78rUTtOZC6mK6jrEGu29p0bS9sxn7-KCuy-4GfB68dqH8JwS0VB1s-UmMjI_6QK5hk1DM)


会議室  
![enter image description here](https://lh3.googleusercontent.com/_ImpWJzzvOb55QVphD5SkRh-W7VZyGukYbEOv9TdufQiX2NzCWVALICU5ZZcpKXq89lcct8LztI)
